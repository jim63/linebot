async function db(pool, sql) {
  try {
    const client = await pool.connect();
    const result = await client.query(sql);
    const results = result ? result.rows : [];
    client.release();
    return results;
  } catch (err) {
    console.error(err);
    return [];
  }
}

module.exports = db;
