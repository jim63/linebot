const fileType = require('file-type');
const fs = require('fs');
const querystring = require('querystring');
const fetch = require('node-fetch');
const moment = require('moment');
const db = require('./db');
const { Pool } = require('pg');
const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
  ssl: true
});

function uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

async function HandleFollow(context) {
  const LINE_ACCESS_TOKEN = process.env.LINE_ACCESS_TOKEN;
  const LINE_CHANNEL_SECRET = process.env.LINE_CHANNEL_SECRET;

  const users = await db(pool, 'SELECT * FROM public.user');

  if (context.event.follow.userId) {
    const userId = context.event.follow.userId;

    await fetch(`https://api.line.me/v2/bot/profile/${userId}`, {
      method: 'GET',
      headers: { Authorization: `Bearer ${LINE_ACCESS_TOKEN}`, 'Content-Type': 'application/json' }
    })
      .then(response => response.json())
      .then(d => {
        const user = { name: d.displayName, userId: d.userId, pictureUrl: d.pictureUrl };
        if (!users.find(u => u.user_id === user.userId)) {
          db(
            pool,
            `INSERT INTO public.user (userId, name, pictureUrl)
            VALUES ('${d.userId}', '${d.displayName}', '${d.pictureUrl}');
            `
          );
        } else if (users.find(u => u.userId === user.userId)) {
          db(
            pool,
            `UPDATE public.user
              SET name = '${user.name}',
              picture_url='${user.pictureUrl}'
              WHERE
              user_id='${user.userId}'; `
          );
        }
      })
      .catch(err => {
        console.log('err', err);
      });
  }
}

async function HandleUnfollow(context) {
  console.log(context.event.unfollow);
  // {
  //   type: 'user',
  //   userId: 'U206d25c2ea6bd87c17655609a1c37cb8',
  // }
}

module.exports = async function App(context) {
  console.log(context.event);
  const clinics = await db(pool, 'SELECT * FROM public.clinic');
  const appointments = await db(pool, 'SELECT * FROM public.appointment');
  const users = await db(pool, 'SELECT * FROM public.user');
  const userId = context.event.source.userId;
  const user = users.find(u => u.user_id === userId);
  const isMaster = user ? (user.master ? true : false) : false;
  const master_id = users.find(u => u.master) ? users.find(u => u.master).user_id : undefined;

  if (context.event.isText) {
    if (context.event.message.text === '追蹤清單') {
      if (isMaster) {
        await context.sendFlex('This is a carousel flex', {
          type: 'carousel',
          contents: users.map(u => {
            return {
              type: 'bubble',
              hero: {
                type: 'image',
                url: u.picture_url
                  ? u.picture_url
                  : 'https://gravatar.com/avatar/62f66206928717f1509e389014a3bd60?s=400&d=robohash&r=x',
                size: 'full'
              },
              body: {
                type: 'box',
                layout: 'vertical',
                contents: [
                  {
                    type: 'text',
                    text: u.name,
                    weight: 'bold',
                    size: 'xl'
                  }
                ]
              }
            };
          })
        });
        return;
      }
      await context.sendText(`received the text message: ${context.event.text}`);
      return;
    } else if (context.event.message.text === '我是誰') {
      if (isMaster) {
        await context.sendText(`u'r monster`);
        return;
      }
    } else if (context.event.message.text === '我的診所') {
      if (user) {
        const clinicId = user.clinic_id;
        const clinic = clinics.find(c => c.id === clinicId);
        await context.sendLocation({
          title: clinic.name,
          address: clinic.address,
          latitude: clinic.latitude,
          longitude: clinic.longitude
        });

        const quickReply = {
          items: [
            {
              type: 'action',
              action: {
                type: 'camera',
                label: '我想自拍'
              }
            },
            {
              type: 'action',
              action: {
                type: 'location',
                label: '附近的診所'
              }
            },
            {
              type: 'action',
              action: {
                type: 'message',
                label: '我想預約',
                text: '我想預約'
              }
            }
          ]
        };

        await context.send([
          {
            type: 'text',
            text: 'hello',
            quickReply
          }
        ]);

        return;
      }
    } else if (context.event.message.text === '我想預約') {
      if (user) {
        if (user.clinic_id) {
          const quickReply = {
            items: [
              {
                type: 'action',
                action: {
                  type: 'datetimepicker',
                  label: 'Select date',
                  data: `action=makeAppoinement&userId=${userId}&clinicId=${user.clinic_id}`,
                  mode: 'datetime'
                }
              }
            ]
          };
          await context.send([
            {
              type: 'text',
              text: '選擇時間',
              quickReply
            }
          ]);
          return;
        } else {
          await context.sendText(`請先新增診所`);
        }
      }
    } else if (context.event.message.text === '我的預約') {
      if (user) {
        const filteredAppt = appointments.filter(a => a.user_id === user.user_id);
        if (filteredAppt.length > 0) {
          await context.sendFlex('This is a carousel flex', {
            type: 'carousel',
            contents: filteredAppt.map(a => {
              const clinic = clinics.find(c => c.id === a.clinic_id);
              const clinicName = clinic ? clinic.name : ' ';
              const clinicUrl = clinic ? clinic.picture_url : undefined;

              return {
                type: 'bubble',
                hero: {
                  type: 'image',
                  url: clinicUrl
                    ? clinicUrl
                    : 'https://gravatar.com/avatar/62f66206928717f1509e389014a3bd60?s=400&d=robohash&r=x',
                  size: 'full'
                },
                body: {
                  type: 'box',
                  layout: 'vertical',
                  contents: [
                    {
                      type: 'text',
                      text: `${clinicName}`,
                      weight: 'bold',
                      size: 'xl'
                    },
                    {
                      type: 'text',
                      text: a.datetime,
                      weight: 'bold',
                      size: 'xl'
                    }
                  ]
                }
              };
            })
          });
          return;
        } else {
          await context.sendText(`你沒有預約`);
        }
        return;
      }
    } else if (context.event.message.text === '更換診所') {
      const template = {
        type: 'buttons',
        thumbnailImageUrl:
          'https://images.unsplash.com/photo-1532119421444-cb013aaed2e7?ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80',
        title: '更換診所',
        text: '選擇你的診所',
        actions: clinics.map(c => ({
          type: 'postback',
          label: c.name,
          data: `action=addClinic&itemid=${c.id}`
        }))
      };
      const altText = 'this is a template';
      await context.sendTemplate(altText, template);
      return;
    }
    await context.sendText(`received the text message: ${context.event.text}`);
  }

  if (context.event.isImage || context.event.isVideo || context.event.isAudio) {
    const buffer = await context.getMessageContent();
    const { ext } = fileType(buffer);
    const filename = `${uuid()}.${ext}`;

    // You can do whatever you want, for example, write buffer into file system
    await fs.promises.writeFile(filename, buffer);
    return;
  }

  if (context.event.isFollow) {
    const template = {
      type: 'buttons',
      thumbnailImageUrl:
        'https://images.unsplash.com/photo-1532119421444-cb013aaed2e7?ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80',
      title: '第一次登入',
      text: '選擇你的診所',
      actions: clinics.map(c => ({
        type: 'postback',
        label: c.name,
        data: `action=addClinic&itemid=${c.id}`
      }))
    };
    const altText = 'this is a template';
    await context.sendTemplate(altText, template);
    return HandleFollow;
  }

  if (context.event.isUnfollow) {
    return HandleUnfollow;
  }

  if (context.event.isPostback) {
    const query = querystring.parse(context.event.postback.data);
    const params = context.event.postback.params
      ? JSON.parse(JSON.stringify(context.event.postback.params))
      : undefined;
    const userId = context.event.source.userId;

    if (query.action === 'addClinic') {
      const clinic = clinics.find(c => c.id == query.itemid).name;
      await db(
        pool,
        `UPDATE public.user
          SET clinic_id = '${query.itemid}'
          WHERE
          user_id='${userId}'; `
      );
      await context.sendText(`設定診所為${clinic}`);
      return;
    } else if (query.action === 'makeAppoinement') {
      if (params) {
        const datetime = params.datetime;
        if (datetime) {
          const userId = query.userId;
          const user = users.find(u => u.user_id === userId);
          const userName = user ? user.name : undefined;

          const clinicId = query.clinicId;
          const clinic = clinics.find(c => c.id === clinicId);
          const clinicName = clinic ? clinic.name : undefined;

          const datetimeString = moment(datetime).format('YYYY-MM-DD HH:MM');

          await db(
            pool,
            `INSERT INTO public.appointment (clinic_id, user_id, datetime)
            VALUES ('${clinicId}', '${userId}', '${datetimeString}');
            `
          );
          await context.sendText(`新增預約成功\n${userName} ${clinicName} ${datetimeString}`);
          await context.client.pushText(master_id, `新增預約\n${userName} ${clinicName} ${datetimeString}`);
        }
      }
    }
  }
};
